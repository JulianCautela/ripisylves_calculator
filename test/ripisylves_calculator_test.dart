import 'package:ripisylves_calculator/ripisylves_calculator.dart';
import 'package:test/test.dart';

void main() {
  group('indice A', () {
    test('2 bois', () {
      expect(Calculator.indiceA(true, true), 5);
    });
    test('bois dur', () {
      expect(Calculator.indiceA(true, false), 2);
    });
    test('bois tendre', () {
      expect(Calculator.indiceA(false, true), 2);
    });
    test('pas de bois', () {
      expect(Calculator.indiceA(false, false), 0);
    });
  });

  group('indice B', () {
    test('4 strates', () {
      expect(Calculator.indiceB(true, true, true, true), 5);
    });
    test('3 strates', () {
      expect(Calculator.indiceB(true, false, true, true), 2);
      expect(Calculator.indiceB(true, true, true, false), 2);
      expect(Calculator.indiceB(true, true, false, true), 2);
      expect(Calculator.indiceB(false, true, true, true), 2);
    });
    test('2 strates', () {
      expect(Calculator.indiceB(false, false, true, true), 2);
      expect(Calculator.indiceB(true, true, false, false), 2);
      expect(Calculator.indiceB(false, true, true, false), 2);
      expect(Calculator.indiceB(false, true, false, true), 2);
      expect(Calculator.indiceB(true, false, true, false), 2);
    });
    test('pas de bois ou 1', () {
      expect(Calculator.indiceB(false, false, false, false), 0);
      expect(Calculator.indiceB(true, false, false, false), 0);
      expect(Calculator.indiceB(false, true, false, false), 0);
      expect(Calculator.indiceB(false, false, true, false), 0);
      expect(Calculator.indiceB(false, false, false, true), 0);
    });
  });

  group('indice C', () {
    List<int> rangeBmmPlein = List.generate(3, (index) => index + 4);
    List<int> rangeBmgPlein = List.generate(6, (index) => index + 4);
    List<int> rangeBmmPoints = List.generate(3, (index) => index + 2);
    //List<int> rangeBmgPoints = List.generate(4, (index) => index + 2);

    group('Plein', () {
      Parcours plein = Parcours.enPlein;
      test('Bmg >= 8', () {
        for (int v in rangeBmmPlein) {
          expect(Calculator.indiceC(plein, 8, v), 5);
          expect(Calculator.indiceC(plein, 9, v), 5);
        }
      });
      test('Bmg [5 - 8]', () {
        for (int v in rangeBmgPlein.takeWhile((value) => 5 <= value && value < 8)) {
          for (int w in rangeBmmPlein) {
            expect(Calculator.indiceC(plein, v, w), 2);
          }
        }
      });

      test('Bmg < 5', () {
        expect(Calculator.indiceC(plein, 4, 4), 0);
        expect(Calculator.indiceC(plein, 4, 5), 1);
        expect(Calculator.indiceC(plein, 4, 6), 1);
      });
    });
    group('Point', () {
      Parcours points = Parcours.enPoints;
      test('Bmg >= 4', () {
        for (int v in rangeBmmPoints) {
          expect(Calculator.indiceC(points, 4, v), 5);
          expect(Calculator.indiceC(points, 5, v), 5);
        }
      });
      test('Bmg == 3', () {
        for (int v in rangeBmmPoints) {
          expect(Calculator.indiceC(points, 3, v), 2);
        }
      });
      test('Bmg < 3', () {
        expect(Calculator.indiceC(points, 2, 2), 0);
        expect(Calculator.indiceC(points, 2, 3), 1);
        expect(Calculator.indiceC(points, 2, 4), 1);
      });
    });
  });

  group('indice D', () {
    List<int> rangeBmmPlein = List.generate(3, (index) => index + 4);
    List<int> rangeTDBPlein = List.generate(3, (index) => index + 4);
    List<int> rangeBmgPlein = List.generate(6, (index) => index + 4);
    List<int> rangeBmmPoints = List.generate(3, (index) => index + 2);
    List<int> rangeTDBPoints = List.generate(3, (index) => index + 2);
    //List<int> rangeBmgPoints = List.generate(4, (index) => index + 2);

    group('Plein', () {
      Parcours plein = Parcours.enPlein;
      test('Bmg >= 8', () {
        for (int bmm in rangeBmmPlein) {
          for (int tdb in rangeTDBPlein) {
            expect(Calculator.indiceD(plein, 8, bmm, tdb), 5);
            expect(Calculator.indiceD(plein, 9, bmm, tdb), 5);
          }
        }
      });
      test('Bmg [5 - 8[', () {
        for (int bmg in rangeBmgPlein.takeWhile((value) => 5 <= value && value < 8)) {
          for (int bmm in rangeBmmPlein) {
            for (int tdb in rangeTDBPlein) {
              expect(Calculator.indiceD(plein, bmg, bmm, tdb), 2);
            }
          }
        }
      });

      test('Bmg < 5 ', () {
        expect(Calculator.indiceD(plein, 4, 4, 4), 0);
        expect(Calculator.indiceD(plein, 4, 5, 4), 1);
        expect(Calculator.indiceD(plein, 4, 6, 4), 1);

        expect(Calculator.indiceD(plein, 4, 4, 5), 1);
        expect(Calculator.indiceD(plein, 4, 5, 5), 1);
        expect(Calculator.indiceD(plein, 4, 6, 5), 1);

        expect(Calculator.indiceD(plein, 4, 4, 6), 1);
        expect(Calculator.indiceD(plein, 4, 5, 6), 1);
        expect(Calculator.indiceD(plein, 4, 6, 6), 1);
      });
    });
    group('Point', () {
      Parcours points = Parcours.enPoints;
      test('Bmg >= 4', () {
        for (int bmm in rangeBmmPoints) {
          for (int tdb in rangeTDBPoints) {
            expect(Calculator.indiceD(points, 4, bmm, tdb), 5);
            expect(Calculator.indiceD(points, 5, bmm, tdb), 5);
          }
        }
      });
      test('Bmg == 3', () {
        for (int bmm in rangeBmmPoints) {
          for (int tdb in rangeTDBPoints) {
            expect(Calculator.indiceD(points, 3, bmm, tdb), 2);
          }
        }
      });
      test('Bmg < 3', () {
        expect(Calculator.indiceD(points, 2, 2, 2), 0);
        expect(Calculator.indiceD(points, 2, 3, 2), 1);
        expect(Calculator.indiceD(points, 2, 4, 2), 1);

        expect(Calculator.indiceD(points, 2, 2, 3), 1);
        expect(Calculator.indiceD(points, 2, 3, 3), 1);
        expect(Calculator.indiceD(points, 2, 4, 3), 1);

        expect(Calculator.indiceD(points, 2, 2, 4), 1);
        expect(Calculator.indiceD(points, 2, 3, 4), 1);
        expect(Calculator.indiceD(points, 2, 4, 4), 1);
      });
    });
  });

  group('indice E', () {
    List<int> rangeGBPlein = List.generate(3, (index) => index + 4);
    List<int> rangeTGBPlein = List.generate(8, (index) => index + 4);
    List<int> rangeGBPoints = List.generate(3, (index) => index + 2);
    List<int> rangeTGBPoints = List.generate(5, (index) => index + 2);

    group('Plein', () {
      Parcours plein = Parcours.enPlein;
      test('TGB >= 10', () {
        for (int gb in rangeGBPlein) {
          expect(Calculator.indiceE(plein, gb, 10), 5);
          expect(Calculator.indiceE(plein, gb, 11), 5);
        }
      });
      test('TGB [5 - 10[', () {
        for (int tgb in rangeTGBPlein.takeWhile((value) => 5 <= value && value < 10)) {
          for (int gb in rangeGBPlein) {
            expect(Calculator.indiceE(plein, gb, tgb), 2);
          }
        }
      });

      test('TGB < 5', () {
        expect(Calculator.indiceE(plein, 4, 4), 0);
        expect(Calculator.indiceE(plein, 5, 4), 1);
        expect(Calculator.indiceE(plein, 6, 4), 1);
      });
    });
    group('Point', () {
      Parcours points = Parcours.enPoints;
      test('TGB >= 5', () {
        for (int gb in rangeGBPoints) {
          expect(Calculator.indiceE(points, gb, 5), 5);
          expect(Calculator.indiceE(points, gb, 6), 5);
        }
      });

      test('TGB [3 - 5[', () {
        for (int tgb in rangeTGBPoints.takeWhile((value) => 3 <= value && value < 5)) {
          for (int gb in rangeGBPoints) {
            expect(Calculator.indiceE(points, gb, tgb), 2);
          }
        }
      });
      test('TGB < 3', () {
        expect(Calculator.indiceE(points, 2, 2), 0);
        expect(Calculator.indiceE(points, 3, 2), 1);
        expect(Calculator.indiceE(points, 4, 2), 1);
      });
    });
  });

  group('indice F', () {
    group('Plein', () {
      Parcours plein = Parcours.enPlein;
      test('dendro >= 10', () {
        expect(Calculator.indiceF(plein, 10), 5);
        expect(Calculator.indiceF(plein, 11), 5);
      });
      test('dendro [5 - 10[', () {
        expect(Calculator.indiceF(plein, 5), 2);
        expect(Calculator.indiceF(plein, 6), 2);
        expect(Calculator.indiceF(plein, 7), 2);
        expect(Calculator.indiceF(plein, 8), 2);
        expect(Calculator.indiceF(plein, 9), 2);
      });
      test('dendro < 5', () {
        expect(Calculator.indiceF(plein, 4), 0);
      });
    });
    group('Point', () {
      Parcours points = Parcours.enPoints;
      test('dendro >= 5', () {
        expect(Calculator.indiceF(points, 5), 5);
        expect(Calculator.indiceF(points, 6), 5);
      });
      test('dendro [3 - 5[', () {
        expect(Calculator.indiceF(points, 3), 2);
        expect(Calculator.indiceF(points, 4), 2);
      });
      test('dendro < 3', () {
        expect(Calculator.indiceF(points, 2), 0);
      });
    });
  });

  group('indice G', () {
    group('Plein', () {
      Parcours plein = Parcours.enPlein;
      test('abris >= 10', () {
        expect(Calculator.indiceG(plein, 10), 5);
        expect(Calculator.indiceG(plein, 11), 5);
      });
      test('abris [5 - 10[', () {
        expect(Calculator.indiceG(plein, 5), 2);
        expect(Calculator.indiceG(plein, 6), 2);
        expect(Calculator.indiceG(plein, 7), 2);
        expect(Calculator.indiceG(plein, 8), 2);
        expect(Calculator.indiceG(plein, 9), 2);
      });
      test('abris < 5', () {
        expect(Calculator.indiceG(plein, 4), 0);
      });
    });
    group('Point', () {
      Parcours points = Parcours.enPoints;
      test('abris >= 5', () {
        expect(Calculator.indiceG(points, 5), 5);
        expect(Calculator.indiceG(points, 6), 5);
      });
      test('abris [3 - 5[', () {
        expect(Calculator.indiceG(points, 3), 2);
        expect(Calculator.indiceG(points, 4), 2);
      });
      test('abris < 3', () {
        expect(Calculator.indiceG(points, 2), 0);
      });
    });
  });

  group('indice H', () {
    test("foret ancienne", () => expect(Calculator.indiceH(Foret.foretAncienne), 5));
    test("foret ancienne probable ou défricher", () => expect(Calculator.indiceH(Foret.foretAncienneProbableOuDefricheeEnPartie), 2));
    test("pas foret acienne", () => expect(Calculator.indiceH(Foret.pasForetAncienne), 0));
  });

  group('indice I', () {
    test("type >= 2", () {
      expect(Calculator.indiceI(2), 5);
      expect(Calculator.indiceI(3), 5);
    });
    test("type == 1", () {
      expect(Calculator.indiceI(1), 2);
    });
    test("type == 0", () {
      expect(Calculator.indiceI(0), 0);
    });
  });

  group('indice J', () {
    test("type >= 3", () {
      expect(Calculator.indiceJ(3), 5);
      expect(Calculator.indiceJ(4), 5);
    });
    test("type == 2", () {
      expect(Calculator.indiceJ(2), 2);
    });
    test("type == 0-1", () {
      expect(Calculator.indiceJ(0), 0);
      expect(Calculator.indiceJ(1), 0);
    });
  });

  group('indice K', () {
    test("recouvrement 0%", () {
      expect(Calculator.indiceK(0), 5);
    });
    test("recouvrement <= 1%", () {
      expect(Calculator.indiceK(1), 4);
    });
    test("recouvrement ]1% - 15%]", () {
      for (int i = 2; i <= 15; i += 1) {
        expect(Calculator.indiceK(i), 2);
      }
    });
    test("recouvrement > 15%", () {
      for (int i = 16; i <= 100; i += 1) {
        expect(Calculator.indiceK(i), 0);
      }
    });
  });

  group('indice L', () {
    test("0 type", () {
      expect(Calculator.indiceL(0), 5);
    });
    test("1 type", () {
      expect(Calculator.indiceL(1), 2);
    });
    test("types >= 2", () {
      expect(Calculator.indiceL(2), 0);
      expect(Calculator.indiceL(3), 0);
    });
  });

  group('indice M1', () {
    test("berge > 75%", () {
      for (double i = 75.01; i <= 100; i += 0.01) {
        expect(Calculator.indiceM1(i), 5);
      }
    });
    test("berge ]50 % - 75 %] ", () {
      for (double i = 50.01; i <= 75; i += 0.01) {
        expect(Calculator.indiceM1(i), 4);
      }
      expect(Calculator.indiceM1(75), 4);
    });
    test("berge ]25 % - 50 %] ", () {
      for (double i = 25; i <= 50; i += 0.01) {
        expect(Calculator.indiceM1(i), 2);
      }
      expect(Calculator.indiceM1(50), 2);
    });
    test("berge < 25% ", () {
      for (double i = 0; i < 25; i += 0.01) {
        expect(Calculator.indiceM1(i), 0);
      }
    });
  });

  group('indice M2', () {
    test("longueur infra > 30m ", () {
      expect(Calculator.indiceM2(31), 0);
    });
    test("longueur infra ]15 m - 30 m]  ", () {
      for (int i = 16; i <= 30; i++) {
        expect(Calculator.indiceM2(i), 2);
      }
    });
    test("longueur infra <= 15m  ", () {
      for (int i = 1; i <= 15; i++) {
        expect(Calculator.indiceM2(i), 4);
      }
    });
    test("longueur infra == 0m ", () {
      expect(Calculator.indiceM2(0), 5);
    });
  });

  group('indice N1', () {
    test("Berge complètement déconnectée", () => expect(Calculator.indiceN1(Berge.bergeCompletementDeconnectee), 0));
    test("Berge partiellement déconnectée", () => expect(Calculator.indiceN1(Berge.bergePartiellementDeconnectee), 2));
    test("Berge connectée", () => expect(Calculator.indiceN1(Berge.bergeConnectee), 5));
  });

  group('indice N2', () {
    test("sol nu > 75%", () {
      for (int i = 76; i <= 100; i += 1) {
        expect(Calculator.indiceN2(i), 0);
      }
    });
    test("sol nu ]50 % - 75 %] ", () {
      for (int i = 51; i <= 75; i += 1) {
        expect(Calculator.indiceN2(i), 2);
      }
    });
    test("sol nu [25 % - 50 %] ", () {
      for (int i = 25; i <= 50; i += 1) {
        expect(Calculator.indiceN2(i), 4);
      }
    });
    test("sol nu < 25% ", () {
      for (int i = 0; i < 25; i += 1) {
        expect(Calculator.indiceN2(i), 5);
      }
    });
  });

  group('indice N3', () {
    test("pente > 100%", () {
      for (int i = 101; i <= 200; i += 1) {
        expect(Calculator.indiceN3(i), 0);
      }
    });
    test("pente [20 % - 100 %]", () {
      for (int i = 20; i <= 100; i += 1) {
        expect(Calculator.indiceN3(i), 2);
      }
    });
    test("pente < 20%", () {
      for (int i = 0; i < 20; i += 1) {
        expect(Calculator.indiceN3(i), 5);
      }
    });
  });

  group('indice O1', () {
    test("longueur  > 100m ", () {
      expect(Calculator.indiceO1(101), 0);
    });
    test("longueur  ]50 m - 100 m]  ", () {
      for (int i = 51; i <= 100; i++) {
        expect(Calculator.indiceO1(i), 2);
      }
    });
    test("longueur  <= 50m  ", () {
      for (int i = 1; i <= 50; i++) {
        expect(Calculator.indiceO1(i), 4);
      }
    });
    test("longueur  == 0m ", () {
      expect(Calculator.indiceO1(0), 5);
    });
  });

  group('indice O2', () {
    test("Plusieurs grosses routes / Une autoroute / TGV grillagé", () {
      expect(Calculator.indiceO2(Voie.plusieursGrossesRoutes), 0);
      expect(Calculator.indiceO2(Voie.TGVgrillage), 0);
      expect(Calculator.indiceO2(Voie.autoroute), 0);
    });

    test("Une grosse route / plusieurs petites routes", () {
      expect(Calculator.indiceO2(Voie.grosseRoute), 2);
      expect(Calculator.indiceO2(Voie.plusieursPetitesRoutes), 2);
    });
    test("Une petite route", () {
      expect(Calculator.indiceO2(Voie.petiteRoute), 4);
    });
    test("Aucune route", () {
      expect(Calculator.indiceO2(Voie.aucuneRoute), 5);
    });
  });

  group('indice O3', () {
    test("nombre batiments > 5", () {
      expect(Calculator.indiceO3(6), 0);
    });
    test("nombre batiments 2 - 5", () {
      expect(Calculator.indiceO3(2), 2);
      expect(Calculator.indiceO3(3), 2);
      expect(Calculator.indiceO3(4), 2);
      expect(Calculator.indiceO3(5), 2);
    });
    test("nombre batiment == 1", () {
      expect(Calculator.indiceO3(1), 4);
    });
    test("nombre batiment == 0", () {
      expect(Calculator.indiceO3(0), 5);
    });
  });
}
