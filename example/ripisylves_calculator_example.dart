import 'dart:convert';

import 'package:ripisylves_calculator/ripisylves_calculator.dart';
import 'package:ripisylves_calculator/src/ripisylves_builder_json.dart';

void main() {
  final builder = RipisylvesBuilderJson("Relevee test", Parcours.enPlein);
  builder
      .buildA(
        true,
        false,
      )
      .buildB(
        false,
        true,
        true,
        false,
      )
      .buildC(
        8,
        4,
      )
      .buildD(
        3,
        5,
        5,
      )
      .buildE(
        3,
        3,
      )
      .buildF(
        orificesEtGaleriesDinsectes: 1,
        concavites: 2,
        aubierApparent: 1,
        aubierEtBoisDeCoeurApparents: 1,
      )
      .buildG(6)
      .buildH(Foret.foretAncienneProbableOuDefricheeEnPartie)
      .buildI(sourceOuSuintement: true, confluenceAvecUnAutreCoursDEau: true)
      .buildJ(falaise: true, eboulisInstable: true)
      .buildK(fauxIndigo: 2)
      .buildL()
      .buildM1(5)
      .buildM2(5)
      .buildN1(Berge.bergeCompletementDeconnectee)
      .buildN2(20)
      .buildN3(20)
      .buildO1(70)
      .buildO2(Voie.grosseRoute)
      .buildO3(2);
  String? result = builder.build();
  print(result);
  String source = '''{
  "nomDuReleve": "Relevee test",
  "typeDeParcours": "enPlein",
  "IBC": 47,
  "indices": {
    "A": 2,
    "B": 2,
    "C": 5,
    "D": 1,
    "E": 0,
    "F": 2,
    "G": 2,
    "H": 2,
    "I": 5,
    "J": 2,
    "K": 2,
    "L": 5,
    "M1": 0,
    "M2": 4,
    "N1": 0,
    "N2": 5,
    "N3": 2,
    "O1": 2,
    "O2": 2,
    "O3": 2
  },
  "aBoisTendre": false,
  "aBoisDur": true,
  "helophyte": true,
  "feuillageInferieurA1_5m": true,
  "feuillageDe1_5mA7m": true,
  "feuillageSuperieurA7m": false,
  "boisMortMoyenSurPied": 4,
  "boisMortGrosSurPied": 8,
  "boisMortMoyenAuSol": 5,
  "boisMortGrosAuSol": 3,
  "tasDeBois": 0,
  "grosBoisVivant": 3,
  "tresGrosBoisVivant": 3,
  "logesDePics": 0,
  "cavitesATerreau": 0,
  "orificesEtGaleriesDinsectes": 1,
  "concavites": 2,
  "aubierApparent": 1,
  "aubierEtBoisDeCoeurApparents": 1,
  "boisMortDansLeHouppier": 0,
  "agglomerationsDeGourmandsOuRameaux": 0,
  "loupesEtChancres": 0,
  "sporophoresDeChampignonsPerennes": 0,
  "sporophoresDeChampignonsEphemeres": 0,
  "plantesEtLichensEpiphytesOuParasites": 0,
  "nids": 0,
  "microsols": 0,
  "couleesDeSeveEtResine": 0,
  "abrisRacinaireAquatique": 6,
  "etatBoisee": "foretAncienneProbableOuDefricheeEnPartie",
  "sourceOuSuintement": true,
  "ruisseletFosseHumideNonEntretenuOuPetitCanalLargeur1M": false,
  "confluenceAvecUnAutreCoursDEau": true,
  "brasMortIsoleSecondaire": false,
  "lacOuPlanDEauProfond": false,
  "etangLaguneOuPlanDEauPeuProfond": false,
  "mareOuAutrePetitPointDEau": false,
  "tourbiere": false,
  "zoneMarecageuse": false,
  "falaise": true,
  "grotteOuGouffre": false,
  "rocherDeHauteurInferieureACelleDuPeuplement": false,
  "eboulisInstable": true,
  "chaosDeBlocs2M": false,
  "affleurementDeBancDeGaletsHorsLitMineur": false,
  "plageDeDepotDeSedimentsFinsSuiteACrue": false,
  "bergesMeublesVerticales": false,
  "amoncellementDeBlocsStables": false,
  "ailante": 0,
  "balsamineDeLHimalaya": 0,
  "berceDuCaucase": 0,
  "buddleiaDeDavidArbreAuxPapillons": 0,
  "erableNegundo": 0,
  "fauxIndigo": 2,
  "laurierCerise": 0,
  "raisinDAmerique": 0,
  "renoueesAsiatiquesExRenoueeDuJapon": 0,
  "solidagesExSolidageVergeDOr": 0,
  "topinambour": 0,
  "vigneViergeCommune": 0,
  "robinierFauxAcacia": 0,
  "artificialisationEtDegradationDesBergesPalplanchesEnrochementsEnBetonEtEnPierre": false,
  "artificialisationDesMilieuxCultureAgricoleGestionForestiereIntensiveJardinsEtParcsEntretenusEspacesCloturesInfranchissablesGrillages": false,
  "impermeabilisation": false,
  "frequentationHumaineEtEspecesAssocieesChiensChevauxBovinsBergesFortementErodeesDePlusDe50MDeLongPassageAAGue": false,
  "dechargeSauvageS1M": false,
  "pourcentageM1": 5.0,
  "longueurM2": 5,
  "connexionDuCoursDEauAuLitMajeur": "bergeCompletementDeconnectee",
  "pourcentageDeSolNuSansVegetation": 20,
  "penteMoyenneDeLaBerge": 20,
  "distanceDeLaRipisylveAvecLesCorridorsEcologiquesAProximitee": 70,
  "nombreEtTypeDInfrastructuresDeTransportAProximiteeDeLaRipisylve": "grosseRoute",
  "nombreDeBatimentsAProximiteDeLaRipisylve": 2
}
''';
  var builder2 = RipisylvesBuilderJson.fromJson(jsonDecode(source));
  print(builder2.build());
}
