import 'dart:convert';

enum Parcours {
  enPlein,
  enPoints,
}

enum Foret { pasForetAncienne, foretAncienneProbableOuDefricheeEnPartie, foretAncienne }

enum Berge { bergeConnectee, bergePartiellementDeconnectee, bergeCompletementDeconnectee }

enum Voie {
  plusieursGrossesRoutes,
  autoroute,
  TGVgrillage,
  grosseRoute,
  plusieursPetitesRoutes,
  petiteRoute,
  aucuneRoute,
}

enum PossibleIndice { A, B, C, D, E, F, G, H, I, J, K, L, M1, M2, N1, N2, N3, O1, O2, O3 }

class Releve {
  String? id;
  String nomDuReleve = "";
  Parcours typeDeParcours = Parcours.enPlein;

  int get IBC => indices.values.reduce((value, element) => value + element);
  Map<PossibleIndice, int> indices = Map.fromEntries(PossibleIndice.values.map((e) => MapEntry(e, 0)));

  bool aBoisTendre = false;
  bool aBoisDur = false;
  bool helophyte = false;
  bool feuillageInferieurA1_5m = false;
  bool feuillageDe1_5mA7m = false;
  bool feuillageSuperieurA7m = false;
  int boisMortMoyenSurPied = 0;
  int boisMortGrosSurPied = 0;
  int boisMortMoyenAuSol = 0;
  int boisMortGrosAuSol = 0;
  int tasDeBois = 0;
  int grosBoisVivant = 0;
  int tresGrosBoisVivant = 0;
  int logesDePics = 0;
  int cavitesATerreau = 0;
  int orificesEtGaleriesDinsectes = 0;
  int concavites = 0;
  int aubierApparent = 0;
  int aubierEtBoisDeCoeurApparents = 0;
  int boisMortDansLeHouppier = 0;
  int agglomerationsDeGourmandsOuRameaux = 0;
  int loupesEtChancres = 0;
  int sporophoresDeChampignonsPerennes = 0;
  int sporophoresDeChampignonsEphemeres = 0;
  int plantesEtLichensEpiphytesOuParasites = 0;
  int nids = 0;
  int microsols = 0;
  int couleesDeSeveEtResine = 0;
  int abrisRacinaireAquatique = 0;
  Foret etatBoisee = Foret.pasForetAncienne;
  bool sourceOuSuintement = false;
  bool ruisseletFosseHumideNonEntretenuOuPetitCanalLargeur1M = false;
  bool confluenceAvecUnAutreCoursDEau = false;
  bool brasMortIsoleSecondaire = false;
  bool lacOuPlanDEauProfond = false;
  bool etangLaguneOuPlanDEauPeuProfond = false;
  bool mareOuAutrePetitPointDEau = false;
  bool tourbiere = false;
  bool zoneMarecageuse = false;
  bool falaise = false;
  bool grotteOuGouffre = false;
  bool rocherDeHauteurInferieureACelleDuPeuplement = false;
  bool eboulisInstable = false;
  bool chaosDeBlocs2M = false;
  bool affleurementDeBancDeGaletsHorsLitMineur = false;
  bool plageDeDepotDeSedimentsFinsSuiteACrue = false;
  bool bergesMeublesVerticales = false;
  bool amoncellementDeBlocsStables = false;
  int ailante = 0;
  int balsamineDeLHimalaya = 0;
  int berceDuCaucase = 0;
  int buddleiaDeDavidArbreAuxPapillons = 0;
  int erableNegundo = 0;
  int fauxIndigo = 0;
  int laurierCerise = 0;
  int raisinDAmerique = 0;
  int renoueesAsiatiquesExRenoueeDuJapon = 0;
  int solidagesExSolidageVergeDOr = 0;
  int topinambour = 0;
  int vigneViergeCommune = 0;
  int robinierFauxAcacia = 0;
  bool artificialisationEtDegradationDesBergesPalplanchesEnrochementsEnBetonEtEnPierre = false;
  bool artificialisationDesMilieuxCultureAgricoleGestionForestiereIntensiveJardinsEtParcsEntretenusEspacesCloturesInfranchissablesGrillages = false;
  bool impermeabilisation = false;
  bool frequentationHumaineEtEspecesAssocieesChiensChevauxBovinsBergesFortementErodeesDePlusDe50MDeLongPassageAAGue = false;
  bool dechargeSauvageS1M = false;
  double pourcentageM1 = 0.0;
  int longueurM2 = 0;
  Berge connexionDuCoursDEauAuLitMajeur = Berge.bergeCompletementDeconnectee;
  int pourcentageDeSolNuSansVegetation = 0;
  int penteMoyenneDeLaBerge = 0;
  int distanceDeLaRipisylveAvecLesCorridorsEcologiquesAProximitee = 0;
  Voie nombreEtTypeDInfrastructuresDeTransportAProximiteeDeLaRipisylve = Voie.aucuneRoute;
  int nombreDeBatimentsAProximiteDeLaRipisylve = 0;

  Releve();

  Releve.fromJson(Map<String, dynamic> map) {
    indices = map["indices"].map<PossibleIndice, int>((key, value) => MapEntry(PossibleIndice.values.byName(key), (value ?? 0) as int));
    nomDuReleve = map["nomDuReleve"] ?? 0;
    typeDeParcours = Parcours.values.byName(map["typeDeParcours"] ?? Parcours.enPlein.name);
    aBoisTendre = map["aBoisTendre"] ?? 0;
    aBoisDur = map["aBoisDur"] ?? 0;
    helophyte = map["helophyte"] ?? 0;
    feuillageInferieurA1_5m = map["feuillageInferieurA1_5m"] ?? 0;
    feuillageDe1_5mA7m = map["feuillageDe1_5mA7m"] ?? 0;
    feuillageSuperieurA7m = map["feuillageSuperieurA7m"] ?? 0;
    boisMortMoyenSurPied = map["boisMortMoyenSurPied"] ?? 0;
    boisMortGrosSurPied = map["boisMortGrosSurPied"] ?? 0;
    boisMortMoyenAuSol = map["boisMortMoyenAuSol"] ?? 0;
    boisMortGrosAuSol = map["boisMortGrosAuSol"] ?? 0;
    tasDeBois = map["tasDeBois"] ?? 0;
    grosBoisVivant = map["grosBoisVivant"] ?? 0;
    tresGrosBoisVivant = map["tresGrosBoisVivant"] ?? 0;
    logesDePics = map["logesDePics"] ?? 0;
    cavitesATerreau = map["cavitesATerreau"] ?? 0;
    orificesEtGaleriesDinsectes = map["orificesEtGaleriesDinsectes"] ?? 0;
    concavites = map["concavites"] ?? 0;
    aubierApparent = map["aubierApparent"] ?? 0;
    aubierEtBoisDeCoeurApparents = map["aubierEtBoisDeCoeurApparents"] ?? 0;
    boisMortDansLeHouppier = map["boisMortDansLeHouppier"] ?? 0;
    agglomerationsDeGourmandsOuRameaux = map["agglomerationsDeGourmandsOuRameaux"] ?? 0;
    loupesEtChancres = map["loupesEtChancres"] ?? 0;
    sporophoresDeChampignonsPerennes = map["sporophoresDeChampignonsPerennes"] ?? 0;
    sporophoresDeChampignonsEphemeres = map["sporophoresDeChampignonsEphemeres"] ?? 0;
    plantesEtLichensEpiphytesOuParasites = map["plantesEtLichensEpiphytesOuParasites"] ?? 0;
    nids = map["nids"] ?? 0;
    microsols = map["microsols"] ?? 0;
    couleesDeSeveEtResine = map["couleesDeSeveEtResine"] ?? 0;
    abrisRacinaireAquatique = map["abrisRacinaireAquatique"] ?? 0;
    etatBoisee = Foret.values.byName(map["etatBoisee"] ?? Foret.pasForetAncienne.name);
    sourceOuSuintement = map["sourceOuSuintement"] ?? 0;
    ruisseletFosseHumideNonEntretenuOuPetitCanalLargeur1M = map["ruisseletFosseHumideNonEntretenuOuPetitCanalLargeur1M"] ?? 0;
    confluenceAvecUnAutreCoursDEau = map["confluenceAvecUnAutreCoursDEau"] ?? 0;
    brasMortIsoleSecondaire = map["brasMortIsoleSecondaire"] ?? 0;
    lacOuPlanDEauProfond = map["lacOuPlanDEauProfond"] ?? 0;
    etangLaguneOuPlanDEauPeuProfond = map["etangLaguneOuPlanDEauPeuProfond"] ?? 0;
    mareOuAutrePetitPointDEau = map["mareOuAutrePetitPointDEau"] ?? 0;
    tourbiere = map["tourbiere"] ?? 0;
    zoneMarecageuse = map["zoneMarecageuse"] ?? 0;
    falaise = map["falaise"] ?? 0;
    grotteOuGouffre = map["grotteOuGouffre"] ?? 0;
    rocherDeHauteurInferieureACelleDuPeuplement = map["rocherDeHauteurInferieureACelleDuPeuplement"] ?? 0;
    eboulisInstable = map["eboulisInstable"] ?? 0;
    chaosDeBlocs2M = map["chaosDeBlocs2M"] ?? 0;
    affleurementDeBancDeGaletsHorsLitMineur = map["affleurementDeBancDeGaletsHorsLitMineur"] ?? 0;
    plageDeDepotDeSedimentsFinsSuiteACrue = map["plageDeDepotDeSedimentsFinsSuiteACrue"] ?? 0;
    bergesMeublesVerticales = map["bergesMeublesVerticales"] ?? 0;
    amoncellementDeBlocsStables = map["amoncellementDeBlocsStables"] ?? 0;
    ailante = map["ailante"] ?? 0;
    balsamineDeLHimalaya = map["balsamineDeLHimalaya"] ?? 0;
    berceDuCaucase = map["berceDuCaucase"] ?? 0;
    buddleiaDeDavidArbreAuxPapillons = map["buddleiaDeDavidArbreAuxPapillons"] ?? 0;
    erableNegundo = map["erableNegundo"] ?? 0;
    fauxIndigo = map["fauxIndigo"] ?? 0;
    laurierCerise = map["laurierCerise"] ?? 0;
    raisinDAmerique = map["raisinDAmerique"] ?? 0;
    renoueesAsiatiquesExRenoueeDuJapon = map["renoueesAsiatiquesExRenoueeDuJapon"] ?? 0;
    solidagesExSolidageVergeDOr = map["solidagesExSolidageVergeDOr"] ?? 0;
    topinambour = map["topinambour"] ?? 0;
    vigneViergeCommune = map["vigneViergeCommune"] ?? 0;
    robinierFauxAcacia = map["robinierFauxAcacia"] ?? 0;
    artificialisationEtDegradationDesBergesPalplanchesEnrochementsEnBetonEtEnPierre = map["artificialisationEtDegradationDesBergesPalplanchesEnrochementsEnBetonEtEnPierre"] ?? 0;
    artificialisationDesMilieuxCultureAgricoleGestionForestiereIntensiveJardinsEtParcsEntretenusEspacesCloturesInfranchissablesGrillages =
        map["artificialisationDesMilieuxCultureAgricoleGestionForestiereIntensiveJardinsEtParcsEntretenusEspacesCloturesInfranchissablesGrillages"] ?? 0;
    impermeabilisation = map["impermeabilisation"] ?? 0;
    frequentationHumaineEtEspecesAssocieesChiensChevauxBovinsBergesFortementErodeesDePlusDe50MDeLongPassageAAGue =
        map["frequentationHumaineEtEspecesAssocieesChiensChevauxBovinsBergesFortementErodeesDePlusDe50MDeLongPassageAAGue"] ?? 0;
    dechargeSauvageS1M = map["dechargeSauvageS1M"] ?? 0;
    pourcentageM1 = map["pourcentageM1"] ?? 0;
    longueurM2 = map["longueurM2"] ?? 0;
    connexionDuCoursDEauAuLitMajeur = Berge.values.byName(map["connexionDuCoursDEauAuLitMajeur"]?? Berge.bergeCompletementDeconnectee) ;
    pourcentageDeSolNuSansVegetation = map["pourcentageDeSolNuSansVegetation"] ?? 0;
    penteMoyenneDeLaBerge = map["penteMoyenneDeLaBerge"] ?? 0;
    distanceDeLaRipisylveAvecLesCorridorsEcologiquesAProximitee = map["distanceDeLaRipisylveAvecLesCorridorsEcologiquesAProximitee"] ?? 0;
    nombreEtTypeDInfrastructuresDeTransportAProximiteeDeLaRipisylve = Voie.values.byName(map["nombreEtTypeDInfrastructuresDeTransportAProximiteeDeLaRipisylve"] ?? Voie.aucuneRoute.name);
    nombreDeBatimentsAProximiteDeLaRipisylve = map["nombreDeBatimentsAProximiteDeLaRipisylve"] ?? 0;
  }

  String toJson() {
    Map<String, dynamic> map = {
      "nomDuReleve": nomDuReleve,
      "typeDeParcours": typeDeParcours.name,
      "IBC": IBC,
      "indices": indices.map((key, value) => MapEntry(key.name, value)),
      "aBoisTendre": aBoisTendre,
      "aBoisDur": aBoisDur,
      "helophyte": helophyte,
      "feuillageInferieurA1_5m": feuillageInferieurA1_5m,
      "feuillageDe1_5mA7m": feuillageDe1_5mA7m,
      "feuillageSuperieurA7m": feuillageSuperieurA7m,
      "boisMortMoyenSurPied": boisMortMoyenSurPied,
      "boisMortGrosSurPied": boisMortGrosSurPied,
      "boisMortMoyenAuSol": boisMortMoyenAuSol,
      "boisMortGrosAuSol": boisMortGrosAuSol,
      "tasDeBois": tasDeBois,
      "grosBoisVivant": grosBoisVivant,
      "tresGrosBoisVivant": tresGrosBoisVivant,
      "logesDePics": logesDePics,
      "cavitesATerreau": cavitesATerreau,
      "orificesEtGaleriesDinsectes": orificesEtGaleriesDinsectes,
      "concavites": concavites,
      "aubierApparent": aubierApparent,
      "aubierEtBoisDeCoeurApparents": aubierEtBoisDeCoeurApparents,
      "boisMortDansLeHouppier": boisMortDansLeHouppier,
      "agglomerationsDeGourmandsOuRameaux": agglomerationsDeGourmandsOuRameaux,
      "loupesEtChancres": loupesEtChancres,
      "sporophoresDeChampignonsPerennes": sporophoresDeChampignonsPerennes,
      "sporophoresDeChampignonsEphemeres": sporophoresDeChampignonsEphemeres,
      "plantesEtLichensEpiphytesOuParasites": plantesEtLichensEpiphytesOuParasites,
      "nids": nids,
      "microsols": microsols,
      "couleesDeSeveEtResine": couleesDeSeveEtResine,
      "abrisRacinaireAquatique": abrisRacinaireAquatique,
      "etatBoisee": etatBoisee.name,
      "sourceOuSuintement": sourceOuSuintement,
      "ruisseletFosseHumideNonEntretenuOuPetitCanalLargeur1M": ruisseletFosseHumideNonEntretenuOuPetitCanalLargeur1M,
      "confluenceAvecUnAutreCoursDEau": confluenceAvecUnAutreCoursDEau,
      "brasMortIsoleSecondaire": brasMortIsoleSecondaire,
      "lacOuPlanDEauProfond": lacOuPlanDEauProfond,
      "etangLaguneOuPlanDEauPeuProfond": etangLaguneOuPlanDEauPeuProfond,
      "mareOuAutrePetitPointDEau": mareOuAutrePetitPointDEau,
      "tourbiere": tourbiere,
      "zoneMarecageuse": zoneMarecageuse,
      "falaise": falaise,
      "grotteOuGouffre": grotteOuGouffre,
      "rocherDeHauteurInferieureACelleDuPeuplement": rocherDeHauteurInferieureACelleDuPeuplement,
      "eboulisInstable": eboulisInstable,
      "chaosDeBlocs2M": chaosDeBlocs2M,
      "affleurementDeBancDeGaletsHorsLitMineur": affleurementDeBancDeGaletsHorsLitMineur,
      "plageDeDepotDeSedimentsFinsSuiteACrue": plageDeDepotDeSedimentsFinsSuiteACrue,
      "bergesMeublesVerticales": bergesMeublesVerticales,
      "amoncellementDeBlocsStables": amoncellementDeBlocsStables,
      "ailante": ailante,
      "balsamineDeLHimalaya": balsamineDeLHimalaya,
      "berceDuCaucase": berceDuCaucase,
      "buddleiaDeDavidArbreAuxPapillons": buddleiaDeDavidArbreAuxPapillons,
      "erableNegundo": erableNegundo,
      "fauxIndigo": fauxIndigo,
      "laurierCerise": laurierCerise,
      "raisinDAmerique": raisinDAmerique,
      "renoueesAsiatiquesExRenoueeDuJapon": renoueesAsiatiquesExRenoueeDuJapon,
      "solidagesExSolidageVergeDOr": solidagesExSolidageVergeDOr,
      "topinambour": topinambour,
      "vigneViergeCommune": vigneViergeCommune,
      "robinierFauxAcacia": robinierFauxAcacia,
      "artificialisationEtDegradationDesBergesPalplanchesEnrochementsEnBetonEtEnPierre": artificialisationEtDegradationDesBergesPalplanchesEnrochementsEnBetonEtEnPierre,
      "artificialisationDesMilieuxCultureAgricoleGestionForestiereIntensiveJardinsEtParcsEntretenusEspacesCloturesInfranchissablesGrillages":
          artificialisationDesMilieuxCultureAgricoleGestionForestiereIntensiveJardinsEtParcsEntretenusEspacesCloturesInfranchissablesGrillages,
      "impermeabilisation": impermeabilisation,
      "frequentationHumaineEtEspecesAssocieesChiensChevauxBovinsBergesFortementErodeesDePlusDe50MDeLongPassageAAGue":
          frequentationHumaineEtEspecesAssocieesChiensChevauxBovinsBergesFortementErodeesDePlusDe50MDeLongPassageAAGue,
      "dechargeSauvageS1M": dechargeSauvageS1M,
      "pourcentageM1": pourcentageM1,
      "longueurM2": longueurM2,
      "connexionDuCoursDEauAuLitMajeur": connexionDuCoursDEauAuLitMajeur.name,
      "pourcentageDeSolNuSansVegetation": pourcentageDeSolNuSansVegetation,
      "penteMoyenneDeLaBerge": penteMoyenneDeLaBerge,
      "distanceDeLaRipisylveAvecLesCorridorsEcologiquesAProximitee": distanceDeLaRipisylveAvecLesCorridorsEcologiquesAProximitee,
      "nombreEtTypeDInfrastructuresDeTransportAProximiteeDeLaRipisylve": nombreEtTypeDInfrastructuresDeTransportAProximiteeDeLaRipisylve.name,
      "nombreDeBatimentsAProximiteDeLaRipisylve": nombreDeBatimentsAProximiteDeLaRipisylve,
    };
    return jsonEncode(map);
  }
}
