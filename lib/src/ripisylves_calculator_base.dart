import 'package:ripisylves_calculator/src/model/releve.dart';

class Calculator {
  static int indiceA(bool boisDur, bool boisTendre) {
    if (boisDur && boisTendre) return 5;
    if (boisDur || boisTendre) return 2;
    return 0;
  }

  static int indiceB(bool helophytes, bool feuillageInf1_5m, bool feuillage1_5mA7m, bool feuillageSup7m) {
    int somme = helophytes ? 1 : 0;
    somme += feuillage1_5mA7m ? 1 : 0;
    somme += feuillageInf1_5m ? 1 : 0;
    somme += feuillageSup7m ? 1 : 0;
    if (somme == 4) return 5;
    if (somme == 2 || somme == 3) return 2;
    return 0;
  }

  static int indiceC(Parcours p, int bmg, int bmm) {
    if (p == Parcours.enPlein) {
      if (bmg >= 8) return 5;
      if (bmg >= 5) return 2;
      if (bmm >= 5) return 1;
    } else if (p == Parcours.enPoints) {
      if (bmg >= 4) return 5;
      if (bmg == 3) return 2;
      if (bmm >= 3) return 1;
    }

    return 0;
  }

  static int indiceD(Parcours p, int bmg, int bmm, int tasDeBois) {
    if (p == Parcours.enPlein) {
      if (bmg >= 8) return 5;
      if (bmg >= 5) return 2;
      if (bmm >= 5 || tasDeBois >= 5) return 1;
    } else if (p == Parcours.enPoints) {
      if (bmg >= 4) return 5;
      if (bmg == 3) return 2;
      if (bmm >= 3 || tasDeBois >= 3) return 1;
    }
    return 0;
  }

  static int indiceE(Parcours p, int gb, int tgb) {
    if (p == Parcours.enPlein) {
      if (tgb >= 10) return 5;
      if (tgb >= 5) return 2;
      if (gb >= 5) return 1;
    } else if (p == Parcours.enPoints) {
      if (tgb >= 5) return 5;
      if (tgb >= 3) return 2;
      if (gb >= 3) return 1;
    }
    return 0;
  }

  static int indiceF(Parcours p, int sommeDendromicrohabitats) {
    if (p == Parcours.enPlein) {
      if (sommeDendromicrohabitats >= 10) return 5;
      if (sommeDendromicrohabitats >= 5) return 2;
    } else if (p == Parcours.enPoints) {
      if (sommeDendromicrohabitats >= 5) return 5;
      if (sommeDendromicrohabitats >= 3) return 2;
    }
    return 0;
  }

  static int indiceG(Parcours p, int sommeAbrisRacinaireAquatique) {
    if (p == Parcours.enPlein) {
      if (sommeAbrisRacinaireAquatique >= 10) return 5;
      if (sommeAbrisRacinaireAquatique >= 5) return 2;
    } else if (p == Parcours.enPoints) {
      if (sommeAbrisRacinaireAquatique >= 5) return 5;
      if (sommeAbrisRacinaireAquatique >= 3) return 2;
    }
    return 0;
  }

  static int indiceH(Foret f) {
    if (f == Foret.foretAncienne) return 5;
    if (f == Foret.foretAncienneProbableOuDefricheeEnPartie) return 2;
    return 0;
  }

  static int indiceI(int nombreType) {
    if (nombreType >= 2) return 5;
    if (nombreType == 1) return 2;
    return 0;
  }

  static int indiceJ(int nombreType) {
    if (nombreType >= 3) return 5;
    if (nombreType == 2) return 2;
    return 0;
  }

  static int indiceK(int pourcentageEspecesExotiqueEnvahissante) {
    if (pourcentageEspecesExotiqueEnvahissante == 0) return 5;
    if (pourcentageEspecesExotiqueEnvahissante <= 1) return 4;
    if (pourcentageEspecesExotiqueEnvahissante <= 15) return 2;
    return 0;
  }

  static int indiceL(int nombreType) {
    if (nombreType == 0) return 5;
    if (nombreType == 1) return 2;
    return 0;
  }

  //pourcentageM1 :   % de la berge avec plus de 5 m de large de couvert arboré ou arbustif (prendre en compte le houppier) :
  static int indiceM1(double pourcentageM1) {
    if (pourcentageM1 > 75) return 5;
    if (pourcentageM1 > 50) return 4;
    if (pourcentageM1 >= 25) return 2;
    return 0;
  }

  //longueurM2 :   Infrastructures de transport (route goudronnée, voie ferrée) transversales aux cours d’eau en mètres de large cumulés
  static int indiceM2(int longueurM2) {
    if (longueurM2 == 0) return 5;
    if (longueurM2 <= 15) return 4;
    if (longueurM2 <= 30) return 2;
    return 0;
  }

  static int indiceN1(Berge b) {
    if (b == Berge.bergeConnectee) return 5;
    if (b == Berge.bergePartiellementDeconnectee) return 2;
    return 0;
  }

  //pourcentageN2 :   % de sol nu sans végétation :
  static int indiceN2(int pourcentageN2) {
    if (pourcentageN2 < 25) return 5;
    if (pourcentageN2 <= 50) return 4;
    if (pourcentageN2 <= 75) return 2;
    return 0;
  }

  //pourcentageN3 :   pente moyenne de la berge
  static int indiceN3(int pourcentageN3) {
    if (pourcentageN3 < 20) return 5;
    if (pourcentageN3 <= 100) return 2;
    return 0;
  }

  //longueur1 :   Distance de la ripisylve avec les corridors écologiques à proximité :
  static int indiceO1(int longueurO1) {
    if (longueurO1 == 0) return 5;
    if (longueurO1 <= 50) return 4;
    if (longueurO1 <= 100) return 2;
    return 0;
  }

  static int indiceO2(Voie v) {
    switch (v) {
      case Voie.aucuneRoute:
        return 5;
      case Voie.petiteRoute:
        return 4;
      case Voie.plusieursPetitesRoutes:
      case Voie.grosseRoute:
        return 2;
      case Voie.plusieursGrossesRoutes:
      case Voie.autoroute:
      case Voie.TGVgrillage:
      default:
        return 0;
    }
  }

  static int indiceO3(int nombreBatiments) {
    if (nombreBatiments == 0) return 5;
    if (nombreBatiments == 1) return 4;
    if (nombreBatiments <= 5) return 2;
    return 0;
  }
}
