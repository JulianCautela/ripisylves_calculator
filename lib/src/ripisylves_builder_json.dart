

import 'package:ripisylves_calculator/ripisylves_calculator.dart';




class RipisylvesBuilderJson {
  Releve releve;
  String? stringJsonResult;

  RipisylvesBuilderJson(String nomDuRelevee, Parcours parcours): releve = Releve(){
    releve.nomDuReleve = nomDuRelevee;
    releve.typeDeParcours = parcours;
  }

  RipisylvesBuilderJson.fromJson(Map<String, dynamic> map): releve = Releve.fromJson(map);


  String? build() {
    stringJsonResult = releve.toJson();
    return stringJsonResult;
  }

  RipisylvesBuilderJson buildA(bool boisDur, bool boisTendre) {
    releve.aBoisDur = boisDur;
    releve.aBoisTendre = boisTendre;
    releve.indices[PossibleIndice.A] = Calculator.indiceA(boisDur, boisTendre);
    return this;
  }

  RipisylvesBuilderJson buildB(bool helophytes, bool feuillageInf1_5m, bool feuillage1_5mA7m, bool feuillageSup7m) {
    releve.helophyte = feuillageInf1_5m;
    releve.feuillageInferieurA1_5m = feuillageInf1_5m;
    releve.feuillageDe1_5mA7m = feuillage1_5mA7m;
    releve.feuillageSuperieurA7m = feuillageSup7m;
    releve.indices[PossibleIndice.B] = Calculator.indiceB(helophytes, feuillageInf1_5m, feuillage1_5mA7m, feuillageSup7m);
    return this;
  }

  RipisylvesBuilderJson buildC(int bmg, int bmm) {
    releve.boisMortGrosSurPied = bmg;
    releve.boisMortMoyenSurPied = bmm;
    releve.indices[PossibleIndice.C] = Calculator.indiceC(releve.typeDeParcours, bmg, bmm);
    return this;
  }

  RipisylvesBuilderJson buildD(int bmg, int bmm, int tasDeBois) {
    releve.boisMortGrosAuSol = bmg;
    releve.boisMortMoyenAuSol = bmm;
    releve.indices[PossibleIndice.D] = Calculator.indiceD(releve.typeDeParcours, bmg, bmm, tasDeBois);
    return this;
  }

  RipisylvesBuilderJson buildE(int gb, int tgb) {
    releve.grosBoisVivant = gb;
    releve.tresGrosBoisVivant = tgb;
    releve.indices[PossibleIndice.E] = Calculator.indiceE(releve.typeDeParcours, gb, tgb);
    return this;
  }

  RipisylvesBuilderJson buildF({
    int logesDePics = 0,
    int cavitesATerreau = 0,
    int orificesEtGaleriesDinsectes = 0,
    int concavites = 0,
    int aubierApparent = 0,
    int aubierEtBoisDeCoeurApparents = 0,
    int boisMortDansLeHouppier = 0,
    int agglomerationsDeGourmandsOuRameaux = 0,
    int loupesEtChancres = 0,
    int sporophoresDeChampignonsPerennes = 0,
    int sporophoresDeChampignonsEphemeres = 0,
    int plantesEtLichensEpiphytesOuParasites = 0,
    int nids = 0,
    int microsols = 0,
    int couleesDeSeveEtResine = 0,
  }) {

    releve.logesDePics = logesDePics;
    releve.cavitesATerreau = cavitesATerreau;
    releve.orificesEtGaleriesDinsectes = orificesEtGaleriesDinsectes;
    releve.concavites = concavites;
    releve.aubierApparent = aubierApparent;
    releve.aubierEtBoisDeCoeurApparents = aubierEtBoisDeCoeurApparents;
    releve.boisMortDansLeHouppier = boisMortDansLeHouppier;
    releve.agglomerationsDeGourmandsOuRameaux = agglomerationsDeGourmandsOuRameaux;
    releve.loupesEtChancres = loupesEtChancres;
    releve.sporophoresDeChampignonsPerennes = sporophoresDeChampignonsPerennes;
    releve.sporophoresDeChampignonsEphemeres = sporophoresDeChampignonsEphemeres;
    releve.plantesEtLichensEpiphytesOuParasites = plantesEtLichensEpiphytesOuParasites;
    releve.nids = nids;
    releve.microsols = microsols;
    releve.couleesDeSeveEtResine = couleesDeSeveEtResine;
    int somme = logesDePics +
        cavitesATerreau +
        orificesEtGaleriesDinsectes +
        concavites +
        aubierApparent +
        aubierEtBoisDeCoeurApparents +
        boisMortDansLeHouppier +
        agglomerationsDeGourmandsOuRameaux +
        loupesEtChancres +
        sporophoresDeChampignonsPerennes +
        sporophoresDeChampignonsEphemeres +
        plantesEtLichensEpiphytesOuParasites +
        nids +
        microsols +
        couleesDeSeveEtResine;
    releve.indices[PossibleIndice.F] = Calculator.indiceF(releve.typeDeParcours, somme);
    return this;
  }

  RipisylvesBuilderJson buildG(int sommeAbrisRacinaireAquatique) {
    releve.abrisRacinaireAquatique = sommeAbrisRacinaireAquatique;
    releve.indices[PossibleIndice.G] = Calculator.indiceG(releve.typeDeParcours, sommeAbrisRacinaireAquatique);
    return this;
  }

  RipisylvesBuilderJson buildH(Foret f) {
    releve.etatBoisee = f;
    releve.indices[PossibleIndice.H] = Calculator.indiceH(f);
    return this;
  }

  RipisylvesBuilderJson buildI({
    bool sourceOuSuintement = false,
    bool ruisseletFosseHumideNonEntretenuOuPetitCanalLargeur1M = false,
    bool confluenceAvecUnAutreCoursDEau = false,
    bool brasMortIsoleSecondaire = false,
    bool lacOuPlanDEauProfond = false,
    bool etangLaguneOuPlanDEauPeuProfond = false,
    bool mareOuAutrePetitPointDEau = false,
    bool tourbiere = false,
    bool zoneMarecageuse = false,
  }) {
    int somme = 0;
    if (sourceOuSuintement) somme++;
    if (ruisseletFosseHumideNonEntretenuOuPetitCanalLargeur1M) somme++;
    if (confluenceAvecUnAutreCoursDEau) somme++;
    if (brasMortIsoleSecondaire) somme++;
    if (lacOuPlanDEauProfond) somme++;
    if (etangLaguneOuPlanDEauPeuProfond) somme++;
    if (mareOuAutrePetitPointDEau) somme++;
    if (tourbiere) somme++;
    if (zoneMarecageuse) somme++;

    releve.sourceOuSuintement = sourceOuSuintement;
    releve.ruisseletFosseHumideNonEntretenuOuPetitCanalLargeur1M = ruisseletFosseHumideNonEntretenuOuPetitCanalLargeur1M;
    releve.confluenceAvecUnAutreCoursDEau = confluenceAvecUnAutreCoursDEau;
    releve.brasMortIsoleSecondaire = brasMortIsoleSecondaire;
    releve.lacOuPlanDEauProfond = lacOuPlanDEauProfond;
    releve.etangLaguneOuPlanDEauPeuProfond = etangLaguneOuPlanDEauPeuProfond;
    releve.mareOuAutrePetitPointDEau = mareOuAutrePetitPointDEau;
    releve.tourbiere = tourbiere;
    releve.zoneMarecageuse = zoneMarecageuse;
    releve.indices[PossibleIndice.I] = Calculator.indiceI(somme);
    return this;
  }

  RipisylvesBuilderJson buildJ({
    bool falaise = false,
    bool grotteOuGouffre = false,
    bool rocherDeHauteurInferieureACelleDuPeuplement = false,
    bool eboulisInstable = false,
    bool chaosDeBlocs2M = false,
    bool affleurementDeBancDeGaletsHorsLitMineur = false,
    bool plageDeDepotDeSedimentsFinsSuiteACrue = false,
    bool bergesMeublesVerticales = false,
    bool amoncellementDeBlocsStables = false,
  }) {
    int somme = 0;
    if (falaise) somme++;
    if (grotteOuGouffre) somme++;
    if (rocherDeHauteurInferieureACelleDuPeuplement) somme++;
    if (eboulisInstable) somme++;
    if (chaosDeBlocs2M) somme++;
    if (affleurementDeBancDeGaletsHorsLitMineur) somme++;
    if (plageDeDepotDeSedimentsFinsSuiteACrue) somme++;
    if (bergesMeublesVerticales) somme++;
    if (amoncellementDeBlocsStables) somme++;
    releve.falaise = falaise;
    releve.grotteOuGouffre = grotteOuGouffre;
    releve.rocherDeHauteurInferieureACelleDuPeuplement = rocherDeHauteurInferieureACelleDuPeuplement;
    releve.eboulisInstable = eboulisInstable;
    releve.chaosDeBlocs2M = chaosDeBlocs2M;
    releve.affleurementDeBancDeGaletsHorsLitMineur = affleurementDeBancDeGaletsHorsLitMineur;
    releve.plageDeDepotDeSedimentsFinsSuiteACrue = plageDeDepotDeSedimentsFinsSuiteACrue;
    releve.bergesMeublesVerticales = bergesMeublesVerticales;
    releve.amoncellementDeBlocsStables = amoncellementDeBlocsStables;

    releve.indices[PossibleIndice.J] = Calculator.indiceJ(somme);
    return this;
  }

  RipisylvesBuilderJson buildK({
    int ailante = 0,
    int balsamineDeLHimalaya = 0,
    int berceDuCaucase = 0,
    int buddleiaDeDavidArbreAuxPapillons = 0,
    int erableNegundo = 0,
    int fauxIndigo = 0,
    int laurierCerise = 0,
    int raisinDAmerique = 0,
    int renoueesAsiatiquesExRenoueeDuJapon = 0,
    int solidagesExSolidageVergeDOr = 0,
    int topinambour = 0,
    int vigneViergeCommune = 0,
    int robinierFauxAcacia = 0,
  }) {
    int somme = ailante +
        balsamineDeLHimalaya +
        berceDuCaucase +
        buddleiaDeDavidArbreAuxPapillons +
        erableNegundo +
        fauxIndigo +
        laurierCerise +
        raisinDAmerique +
        renoueesAsiatiquesExRenoueeDuJapon +
        solidagesExSolidageVergeDOr +
        topinambour +
        vigneViergeCommune +
        robinierFauxAcacia;
    releve.ailante = ailante;
    releve.balsamineDeLHimalaya = balsamineDeLHimalaya;
    releve.berceDuCaucase = berceDuCaucase;
    releve.buddleiaDeDavidArbreAuxPapillons = buddleiaDeDavidArbreAuxPapillons;
    releve.erableNegundo = erableNegundo;
    releve.fauxIndigo = fauxIndigo;
    releve.laurierCerise = laurierCerise;
    releve.raisinDAmerique = raisinDAmerique;
    releve.renoueesAsiatiquesExRenoueeDuJapon = renoueesAsiatiquesExRenoueeDuJapon;
    releve.solidagesExSolidageVergeDOr = solidagesExSolidageVergeDOr;
    releve.topinambour = topinambour;
    releve.vigneViergeCommune = vigneViergeCommune;
    releve.robinierFauxAcacia = robinierFauxAcacia;
    releve.indices[PossibleIndice.K] = Calculator.indiceK(somme);
    return this;
  }

  RipisylvesBuilderJson buildL({
    bool artificialisationEtDegradationDesBergesPalplanchesEnrochementsEnBetonEtEnPierre = false,
    bool artificialisationDesMilieuxCultureAgricoleGestionForestiereIntensiveJardinsEtParcsEntretenusEspacesCloturesInfranchissablesGrillages = false,
    bool impermeabilisation = false,
    bool frequentationHumaineEtEspecesAssocieesChiensChevauxBovinsBergesFortementErodeesDePlusDe50MDeLongPassageAAGue = false,
    bool dechargeSauvageS1M = false,
  }) {
    int somme = 0;
    if (artificialisationEtDegradationDesBergesPalplanchesEnrochementsEnBetonEtEnPierre) somme++;
    if (artificialisationDesMilieuxCultureAgricoleGestionForestiereIntensiveJardinsEtParcsEntretenusEspacesCloturesInfranchissablesGrillages) somme++;
    if (impermeabilisation) somme++;
    if (frequentationHumaineEtEspecesAssocieesChiensChevauxBovinsBergesFortementErodeesDePlusDe50MDeLongPassageAAGue) somme++;
    if (dechargeSauvageS1M) somme++;

    releve.artificialisationEtDegradationDesBergesPalplanchesEnrochementsEnBetonEtEnPierre = artificialisationEtDegradationDesBergesPalplanchesEnrochementsEnBetonEtEnPierre;
    releve.artificialisationDesMilieuxCultureAgricoleGestionForestiereIntensiveJardinsEtParcsEntretenusEspacesCloturesInfranchissablesGrillages =
        artificialisationDesMilieuxCultureAgricoleGestionForestiereIntensiveJardinsEtParcsEntretenusEspacesCloturesInfranchissablesGrillages;
    releve.impermeabilisation = impermeabilisation;
    releve.frequentationHumaineEtEspecesAssocieesChiensChevauxBovinsBergesFortementErodeesDePlusDe50MDeLongPassageAAGue =
        frequentationHumaineEtEspecesAssocieesChiensChevauxBovinsBergesFortementErodeesDePlusDe50MDeLongPassageAAGue;
    releve.dechargeSauvageS1M = dechargeSauvageS1M;

    releve.indices[PossibleIndice.L] = Calculator.indiceL(somme);
    return this;
  }

//pourcetnageM1 :   % de la berge avec plus de 5 m de large de couvert arboré ou arbustif (prendre en compte le houppier) :
  RipisylvesBuilderJson buildM1(double pourcentageM1) {
    releve.pourcentageM1 = pourcentageM1;

    releve.indices[PossibleIndice.M1] = Calculator.indiceM1(pourcentageM1);
    return this;
  }

//longueurM2 :   Infrastructures de transport (route goudronnée, voie ferrée) transversales aux cours d’eau en mètres de large cumulés
  RipisylvesBuilderJson buildM2(int longueurM2) {
    releve.longueurM2 = longueurM2;

    releve.indices[PossibleIndice.M2] = Calculator.indiceM2(longueurM2);
    return this;
  }

  RipisylvesBuilderJson buildN1(Berge b) {
    releve.connexionDuCoursDEauAuLitMajeur = b;

    releve.indices[PossibleIndice.N1] = Calculator.indiceN1(b);
    return this;
  }

//pourcetnageN2 :   % de sol nu sans végétation :
  RipisylvesBuilderJson buildN2(int pourcentageN2) {
    releve.pourcentageDeSolNuSansVegetation = pourcentageN2;

    releve.indices[PossibleIndice.N2] = Calculator.indiceN2(pourcentageN2);
    return this;
  }

//pourcetnageN3 :   pente moyenne de la berge
  RipisylvesBuilderJson buildN3(int pourcentageN3) {
    releve.penteMoyenneDeLaBerge = pourcentageN3;

    releve.indices[PossibleIndice.N3] = Calculator.indiceN3(pourcentageN3);
    return this;
  }

//longueur1 :   Distance de la ripisylve avec les corridors écologiques à proximité :
  RipisylvesBuilderJson buildO1(int longueurO1) {
    releve.distanceDeLaRipisylveAvecLesCorridorsEcologiquesAProximitee = longueurO1;

    releve.indices[PossibleIndice.O1] = Calculator.indiceO1(longueurO1);
    return this;
  }

  RipisylvesBuilderJson buildO2(Voie v) {
    releve.nombreEtTypeDInfrastructuresDeTransportAProximiteeDeLaRipisylve = v;

    releve.indices[PossibleIndice.O2] = Calculator.indiceO2(v);
    return this;
  }

  RipisylvesBuilderJson buildO3(int nombreBatiments) {
    releve.nombreDeBatimentsAProximiteDeLaRipisylve = nombreBatiments;

    releve.indices[PossibleIndice.O3] = Calculator.indiceO3(nombreBatiments);
    return this;
  }
}
